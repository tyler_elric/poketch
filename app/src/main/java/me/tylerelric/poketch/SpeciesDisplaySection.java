package me.tylerelric.poketch;
import android.content.ContentResolver;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public abstract class SpeciesDisplaySection
extends Fragment
implements onSpeciesChangedListener {

    protected ContentResolver mContentResolver;

    public interface onDisplayAttachedListener {
        public void onDisplayAttached(SpeciesDisplaySection section);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContentResolver = getActivity().getApplicationContext().getContentResolver();
    }

    protected onDisplayAttachedListener displayListener;

    public void setDisplayListener(onDisplayAttachedListener _DL) {
        displayListener = _DL;
    }

    public onDisplayAttachedListener getDisplayListener() {
        return displayListener;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(displayListener!=null) {
            displayListener.onDisplayAttached(this);
        }
    }
}
