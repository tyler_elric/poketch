package me.tylerelric.poketch;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SpeciesEggGroups
extends SpeciesDisplaySection
implements OnResultClicked {

    Cursor egg_groups = null;

    private RecyclerView mRecyclerView;
    private ResultAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public SpeciesEggGroups() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_species_egg_groups, container, false);
        Activity act = getActivity();
        Context ctx = act.getApplicationContext();
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.egg_groups_list);
        mAdapter = new ResultAdapter(egg_groups,OnResultClicked.TAG_EGG_GROUP_RESULT);
        mAdapter.setOnResultClickListener(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void updateSpecies(Cursor cursor) {
        int species_id = cursor.getInt(1);
        egg_groups = mContentResolver.query(
                Uri.parse("content://me.tylerelric.poketch.content/egggroups/" + species_id),
                null,
                null,
                null,
                "+name"
        );
        updateEggGroups();
    }

    private void updateEggGroups() {
        mAdapter = new ResultAdapter(egg_groups,OnResultClicked.TAG_EGG_GROUP_RESULT);
        mAdapter.setOnResultClickListener(this);
        mRecyclerView.swapAdapter(mAdapter,true);
    }

    @Override
    public void onResultClicked(int result_type, int result_value) {
        // launch an egg-group results search.
        // displays all species inside an egg-group.
    }
}
