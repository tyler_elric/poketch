package me.tylerelric.poketch;


import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;

public class SpeciesMoves
extends SpeciesDisplaySection
implements OnResultClicked
{

    Cursor moves_cursor;

    private RecyclerView mRecyclerView;
    private ResultAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public SpeciesMoves() {
    }

    @Override
    public void onResultClicked(int result_type, int result_value) {

    }

    @Override
    public void updateSpecies(Cursor cursor) {
        int species_id = cursor.getInt(1);
        moves_cursor = mContentResolver.query(
                Uri.parse("content://me.tylerelric.poketch.content/moves/" + species_id),
                null,
                null,
                null,
                "+name"
        );
    }
}
