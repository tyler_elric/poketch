package me.tylerelric.poketch;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;


public class SpeciesDetails
extends ActionBarActivity
implements GestureDetector.OnGestureListener,
GestureDetector.OnDoubleTapListener,
SpeciesDisplaySection.onDisplayAttachedListener
{

    int species_id = 1;
    ContentResolver mContentResolver;
    private Cursor species_cursor;
    SpeciesDisplaySection currentDisplay;
    SpeciesGlance speciesGlance;
    SpeciesEggGroups eggGroups;
    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_species_details);
        mContentResolver = getApplicationContext().getContentResolver();
        int species_id = getIntent().getIntExtra("species_id",1);
        if (savedInstanceState == null) {
            species_cursor = mContentResolver.query(
                    Uri.parse("content://me.tylerelric.poketch.content/species/" + species_id),
                    null,
                    null,
                    null,
                    null
            );
            setDisplaySection(R.layout.fragment_species_glance);
        } else {
            species_id = savedInstanceState.getInt("species_id");
            species_cursor = mContentResolver.query(
                    Uri.parse("content://me.tylerelric.poketch.content/species/" + species_id),
                    null,
                    null,
                    null,
                    null
            );
        }
        mDetector = new GestureDetectorCompat(this,this);
        mDetector.setOnDoubleTapListener(this);
        findViewById(R.id.species_details_container).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v,MotionEvent event) {
                return mDetector.onTouchEvent(event);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("species_id",species_id);
    }

    protected void setDisplaySection(int view) {
        currentDisplay = getDisplayView(view);
        if(currentDisplay.getDisplayListener()!=this) {
            currentDisplay.setDisplayListener(this);
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,currentDisplay)
                .commit();
    }

    private SpeciesDisplaySection getDisplayView(int view) {
        switch(view) {
            case R.layout.fragment_species_glance:
                if(speciesGlance==null) {
                    speciesGlance = new SpeciesGlance();
                }
                return speciesGlance;
            case R.layout.fragment_species_egg_groups:
                if(eggGroups==null) {
                    eggGroups = new SpeciesEggGroups();
                }
                return eggGroups;
            default:
                return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_species_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case R.id.egg_groups:
                setDisplaySection(R.layout.fragment_species_egg_groups);
                break;
            case R.id.description:
                setDisplaySection(R.layout.fragment_species_glance);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        int direction = 0;
        if(motionEvent.getX()<motionEvent2.getX()) {
            // left.
            // move cursor back.
            direction = -1;
        } else if(motionEvent.getX()>motionEvent2.getX()) {
            // right.
            // advance cursor.
            direction = 1;
        }
        species_cursor.move(direction);
        if(species_cursor.isBeforeFirst()) {
            species_cursor.moveToLast();
        } else if(species_cursor.isAfterLast()){
            species_cursor.moveToFirst();
        }
        species_id = species_cursor.getInt(1);
        if(direction!=0) {
            setTitle(species_cursor.getString(0));
            updateSpeciesListeners();
            return true;
        }
        return false;
    }

    private void updateSpeciesListeners() {
        currentDisplay.updateSpecies(species_cursor);
    }

    @Override
    public void onDisplayAttached(SpeciesDisplaySection section) {
        section.updateSpecies(species_cursor);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return true;
    }
}
