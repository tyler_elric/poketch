package me.tylerelric.poketch.contentprovider;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Tyler on 12/24/2014.
 */
class Query
implements Comparator<QueryRow> {
    private List<SortDirective> sorts = new ArrayList<SortDirective>();
    private List<FilterDirective> filters = new ArrayList<FilterDirective>();
    private ArrayList<QueryRow> rows = new ArrayList<QueryRow>();
    private MatrixCursor cursor = null;

    public Query(String[] columns) {
        cursor = new MatrixCursor(columns);
    }

    public void filter(FilterDirective fd ){
        filters.add(fd);
    }

    public void sort(SortDirective sd) {
        sorts.add(sd);
    }

    public Cursor build_cursor() {
        QueryRow[] _rows = new QueryRow[0];
        _rows = rows.toArray(_rows);
        Arrays.sort(_rows, this);
        Log.w("PokeApiProvider.Query", "Num rows: " + rows.size());
        for(int i=0;i<_rows.length;i++) {
            QueryRow row = _rows[i];
            if(match(row.data)) {
                row.insert(cursor);
            }
        }
        return cursor;
    }

    public int compare(QueryRow queryRow, QueryRow queryRow2) {
        if(sorts.size()<1) {
            return 0;
        } else {
            for(int i=0;i<sorts.size();i++) {
                SortDirective s = sorts.get(i);
                int score_a = queryRow.evaluateColumn(s.column);
                int score_b = queryRow2.evaluateColumn(s.column);
                int direction = s.ascending?1:-1;
                if(score_a==score_b) {
                    continue;
                }
                return score_a - score_b;
            }
        }
        return 0;
    }

    protected boolean match(Object[] data) {
        for(FilterDirective f:filters) {
            if(! f.match(data)){
                return false;
            }
        }
        return true;
    }

    public QueryRow get(int index) {
        return rows.get(index);
    }

    public void addData(Object[] _data) {
        rows.add(new QueryRow(_data));
    }
}
