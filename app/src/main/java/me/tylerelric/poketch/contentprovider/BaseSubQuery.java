package me.tylerelric.poketch.contentprovider;

import android.database.Cursor;
import android.database.MatrixCursor;

import java.sql.Blob;

public abstract class BaseSubQuery
extends MatrixCursor{

    protected Object[][] data;
    protected final String[] actual_column_names;
    protected final int[] actual_column_types;

    public BaseSubQuery(Cursor inner,final int[] columnTypes,String[] columnNames) {
        super(inner.getColumnNames());
        actual_column_names = columnNames;
        actual_column_types = columnTypes;
        seedData(inner);
    }

    public BaseSubQuery(Cursor inner,final int[] columnTypes,final String[] columnNames, int initialCapacity) {
        super(inner.getColumnNames(), initialCapacity);
        actual_column_names = columnNames;
        actual_column_types = columnTypes;
        seedData(inner);
    }

    private void seedData(Cursor inner) {
        data = new Object[inner.getCount()][];
        Object[] old_data = new Object[inner.getColumnCount()];
        while(!inner.isAfterLast()) {
            for(int i=0;i<old_data.length;i++) {
                switch(inner.getType(i)) {
                    case Cursor.FIELD_TYPE_BLOB:
                        old_data[i] = super.getBlob(i);
                        break;
                    case Cursor.FIELD_TYPE_INTEGER:
                        old_data[i] = super.getInt(i);
                        break;
                    case Cursor.FIELD_TYPE_FLOAT:
                        old_data[i] = super.getFloat(i);
                        break;
                    case Cursor.FIELD_TYPE_STRING:
                        old_data[i] = super.getFloat(i);
                        break;
                    default:
                    case Cursor.FIELD_TYPE_NULL:
                        old_data[i] = null;
                        break;
                }
            }
            super.addRow(old_data.clone());
            inner.moveToNext();
        }
        moveToFirst();
    }

    protected abstract Object[] pull_data();

    protected void ensureData() {
        int pos = getPosition();
        if(data[pos]==null) {
            data[pos] = pull_data();
        }
    }

    protected Object getData(int c) {
        ensureData();
        return data[getPosition()][c];
    }

    @Override
    public int getInt(int c) {
        return (int)getData(c);
    }

    @Override
    public long getLong(int c) {
        return (long) getData(c);
    }

    @Override
    public double getDouble(int c) {
        return (double) getData(c);
    }

    @Override
    public String getString(int c) {
        return (String)getData(c);
    }

    @Override
    public byte[] getBlob(int c) {
        return (byte[]) getData(c);
    }

}
