package me.tylerelric.poketch.contentprovider;

public class QueryResult {
    public final int kind;
    public final int id;
    public QueryResult(int _kind,int _id) {
        kind = _kind;
        id = _id;
    }
}
