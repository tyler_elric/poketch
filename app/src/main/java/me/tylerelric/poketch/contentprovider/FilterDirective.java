package me.tylerelric.poketch.contentprovider;

/**
 * Created by Tyler on 12/24/2014.
 */
class FilterDirective {

    int column;
    int logic;
    Object base;

    public FilterDirective(int c,int t,Object com) {
        column = c;
        logic = t;
        base = com;
    }

    public boolean apply_logic(int score) {
        return true;
        /*if(logic<0) {
            //less than base.
            return score < 0;
        } else if (logic > 0){
            return score > 0;
        } else {
            return score == 0;
        }*/
    }

    public static FilterDirective[] parseString(String s) {
        return new FilterDirective[0];
    }

    public boolean match(Object[] data) {
        if(base instanceof String) {
            return apply_logic(((String) base).compareTo((String) data[column]));
        } else {
            return apply_logic((int)base - (int)data[column]);
        }
    }
}
