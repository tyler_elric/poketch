package me.tylerelric.poketch.contentprovider;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

public abstract class BaseContract {

    public abstract String[] getColumns();
    public abstract int[] getTypes();

    protected Cursor run_query(ContentResolver mContentResolver,String[] parts) {
        parts = parts==null?new String[0]:parts;
        StringBuilder builder = new StringBuilder();
        for(String p:parts) {
            builder.append(p+"/");
        }
        Uri uri = Uri.fromParts("content","",builder.toString());
        return build_cursor(mContentResolver,uri);
    }

    protected abstract Cursor build_cursor(ContentResolver mContentResolver, Uri uri);

    public Query buildQuery(FilterDirective[] filters,SortDirective[] sorts) {
        Query q = new Query(getColumns());
        filters = filters==null?new FilterDirective[0]:filters;
        sorts = sorts==null?new SortDirective[0]:sorts;
        for(FilterDirective f:filters) {
            q.filter(f);
        }
        for(SortDirective s:sorts) {
            q.sort(s);
        }
        return q;
    }

    public Query buildQuery(FilterDirective[] filters) {
        return buildQuery(filters,null);
    }

    public Query buildQuery(SortDirective[] sorts) {
        return buildQuery(null,sorts);
    }

    public Query buildQuery() {
        return buildQuery(null,null);
    }

}
