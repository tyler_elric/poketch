package me.tylerelric.poketch.contentprovider;

import me.tylerelric.pokeapi.ParseFail;
import me.tylerelric.pokeapi.PokeApi;
import me.tylerelric.pokeapi.Wall;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PokeApiProvider extends ContentProvider {


    protected final int POKEDEX_LOOKUP = 0;
    protected final int SPECIES_LOOKUP = 1;
    protected final int ABILITY_LOOKUP = 2;
    protected final int SPECIES_ABILITIES = 3;
    protected final int MOVE_LOOKUP = 4;
    protected final int SPECIES_MOVES = 5;
    protected final int TYPE_LOOKUP = 6;
    protected final int SPECIES_TYPES = 7;
    protected final int EGG_GROUP_LOOKUP = 8;
    protected final int SPECIES_EGG_GROUPS = 9;
    protected final int DESCRIPTION_LOOKUP = 10;
    protected final int SPECIES_DESCRIPTIONS = 11;
    protected final int SPRITE_LOOKUP = 12;
    protected final int SPECIES_SPRITES = 13;
    protected final int GAME_LOOKUP = 14;
    protected final int SPECIES_GAMES = 15;

    protected String[] uris = new String[] {
            "pokedex",//searches through all the pokedex ( names and id's ) for fuzzy matches.
            "species/*",//looks up detailed info about a given pokemon.
            "ability/*",//looks up an ability
            "abilities/*",//looks up all abilities for a given pokemon.
            "move/*",// looks up a a specific move.
            "moves/*",//looks up all moves for a given pokemon.
            "type/*",//looks up type info for a given elemental type.
            "types/*",//looks up detailed info for a given pokemon.
            "egggroup/*",//looks up info for an egg group.
            "egggroups/*",//looks up info for a given pokemon.
            "description/#",
            "descriptions/*",
            "sprite/*",
            "sprites/*",
            "games/#",
            "game/#"
    };
    protected static final String Authority = "me.tylerelric.poketch.content";
    protected UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    PokeApi service;

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Content is read-only.");
    }

    @Override
    public String getType(Uri uri) {
        switch(uriMatcher.match(uri)) {
            default:
                return null;
            case SPECIES_LOOKUP:
                return Authority + "/species";
            case ABILITY_LOOKUP:
                return Authority + "/ability";
            case MOVE_LOOKUP:
                return Authority + "/move";
            case TYPE_LOOKUP:
                return Authority + "/type";
            case EGG_GROUP_LOOKUP:
                return Authority + "/egggroup";
            case DESCRIPTION_LOOKUP:
                return Authority + "/description";
            case SPRITE_LOOKUP:
                return Authority + "/sprite";
            case GAME_LOOKUP:
                return Authority + "/game";
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Content is read-only.");
    }

    @Override
    public boolean onCreate() {
        for(int i=POKEDEX_LOOKUP;i<=SPECIES_GAMES;i++) {
            uriMatcher.addURI(Authority,uris[i-POKEDEX_LOOKUP],i);
        }
        String DIRECTORY = "datacache";
        File rootdir = getContext().getFilesDir();
        File cache_dir = new File(rootdir,DIRECTORY);
        service = new PokeApi(cache_dir, 128);
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor r = null;
        try {
            int _m = uriMatcher.match(uri);
            List<String> parts = uri.getPathSegments();
            FilterDirective[] filters = new FilterDirective[0];
            SortDirective[] sorts = new SortDirective[0];
            Log.i("PokeApiProvider","URI Scheme: " + uri.toString());
            switch (_m) {
                case POKEDEX_LOOKUP:
                    r = searchPokedex(filters,sorts).build_cursor();
                    break;
                case SPECIES_LOOKUP:
                    r = searchSpecies(parts.get(1));
                    break;
                case SPECIES_DESCRIPTIONS:
                    r = searchDescriptionsBySpecies(parts.get(1),filters,sorts);
                    break;
                case SPECIES_EGG_GROUPS:
                    r = searchEggGroupsBySpecies(parts.get(1),filters,sorts);
                    break;
                default:
                    Log.w("PokeApiProvider","Unknown URI Scheme: " +uri.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseFail parseFail) {
            parseFail.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return r;
    }

    protected Query build_query(String[] columns,FilterDirective[] filters,SortDirective[] sorts) {
        Query r = new Query(columns);
        if(sorts!=null){
            for(SortDirective s:sorts) {
                r.sort(s);
            }
        }
        if(filters!=null) {
            for(FilterDirective f:filters) {
                r.filter(f);
            }
        }
        return r;
    }

    protected Query searchPokedex(FilterDirective[] filters, SortDirective[] sortOrders)
    throws JSONException, IOException, ParseFail, InterruptedException, ExecutionException {
        int prefix = "api/v1/".length();
        int prefix_2 = "pokemon/".length();
        Query query = build_query(new String[] {
                "id",
                "name",
                "key"
            },
            filters,
            sortOrders
        );
        query.sort(new SortDirective(0,true));
        JSONArray pokedex = service.getPokedex().getJSONArray("pokemon");
        for(int i=0;i<pokedex.length();i++) {
            JSONObject meta = pokedex.getJSONObject(i);
            String name = meta.getString("name");
            String key = meta.getString("resource_uri").substring(prefix);
            String nat_id = key.substring(prefix_2);
            while(nat_id.charAt(nat_id.length()-1)=='/') {
                nat_id=nat_id.substring(0,nat_id.length()-1);
            }
            int id = Integer.parseInt(nat_id, 10);
            query.addData(new Object[]{
                    id,
                    name,
                    key
                }
            );
        }
        return query;
    }

    protected Cursor searchSpecies(String id)
    throws JSONException, IOException, ParseFail, InterruptedException, ExecutionException {
        Query pokedex_ = searchPokedex(null, null);
        Cursor pokedex = pokedex_.build_cursor();
        Cursor r = new SpeciesCursor(service,pokedex);
        Log.i("PokeApiProvider",id);
        try {
            int species_id = Integer.parseInt(id,10);
            while(!pokedex.isAfterLast()) {
                if(pokedex.getInt(0)==species_id) {
                    r.moveToPosition(pokedex.getPosition());
                    break;
                }
                pokedex.move(1);
            }
            pokedex.moveToFirst();
        } catch (Exception e) {
            e.printStackTrace();
            pokedex.moveToFirst();
            /*while(!pokedex.isAfterLast()) {
                pokedex.move(1);
            }*/
        }
        return r;
    }

    protected Query searchAbility(String selection, String[] selectionArgs, String sortOrder ) {
        return null;
    }

    protected Query searchAbilityBySpecies(String selection, String[] selectionArgs, String sortOrder ) {
        return null;
    }

    protected Query searchMoves(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Cursor searchMovesBySpecies(String species,FilterDirective[] filters,SortDirective[] sorts)
    throws JSONException, IOException, ParseFail, InterruptedException, ExecutionException{
        Query q = build_query(new String[]{
                "id",
                "learn_type",
                "level",
                "category",
                "name",
                "description",
                "accuracy",
                "power",
                "pp"
        },filters,sorts);
        int prefix = "api/v1/".length();
        int prefix_2 = "move/".length();
        int species_id = Integer.parseInt(species);
        JSONObject species_meta = service.getSpecies(species_id);
        JSONArray moveset = species_meta.getJSONArray("moves");
        int[] ids = new int[moveset.length()];
        for(int i=0;i<moveset.length();i++) {
            ids[i] = Integer.parseInt(moveset.getString(i).substring(prefix).substring(prefix_2),10);
        }
        for(int i=0;i<ids.length;i++) {
            int id=ids[0];
            JSONObject move = service.getMove(id);
            JSONObject meta = moveset.getJSONObject(i);
            q.addData(new Object[]{
                    id,
                    meta.getString("learn_type"),
                    meta.has("level")?meta.getInt("level"):null,
                    move.getString("category"),
                    move.getString("name"),
                    move.getString("description"),
                    move.getString("")
            });
        }
        return q.build_cursor();
    }

    protected Query searchTypes(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Query searchTypesBySpecies(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Query searchEggGroups(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Cursor searchEggGroupsBySpecies(String id, FilterDirective[] filters, SortDirective[] sorts)
    throws JSONException, IOException, ParseFail, InterruptedException, ExecutionException {
        Query q = new Query(new String[]{
            "name",
            "id"
        });
        for(FilterDirective filter:filters) {
            q.filter(filter);
        }
        for(SortDirective sort:sorts) {
            q.sort(sort);
        }
        int prefix = "/api/v1/".length();
        int prefix_2 = "egg/".length();
        int species_id = Integer.parseInt(id,10);
        JSONArray egg_groups_meta = service.getSpecies(species_id).getJSONArray("egg_groups");
        for(int i=0;i<egg_groups_meta.length();i++) {
            JSONObject meta = egg_groups_meta.getJSONObject(i);
            String key = meta.getString("resource_uri").substring(prefix);
            String group_id = key.substring(prefix_2);
            q.addData(new Object[]{
                    meta.getString("name"),
                    group_id
            });
        }
        return q.build_cursor();
    }

    protected Query searchDescriptions(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Cursor searchDescriptionsBySpecies(String species_id,FilterDirective[] filters,SortDirective[] sorts)
    throws JSONException, ParseFail, IOException, InterruptedException, ExecutionException {
        Query q = new Query(new String[]{
            "description"
        });
        JSONObject species = service.getSpecies(Integer.parseInt(species_id,10));
        JSONArray descriptions = species.getJSONArray("descriptions");
        int prefix = "/api/v1/".length();
        Wall<JSONObject> wall = new Wall<JSONObject>(service);
        for(int i=0;i<descriptions.length();i++) {
            JSONObject meta = descriptions.getJSONObject(i);
            String key = meta.getString("resource_uri");
            key = key.substring(prefix);
            wall.prepare(key);
        }
        Object[] results = wall.get_all();
        for(Object obj:results) {
            JSONObject jobj = (JSONObject) obj;
            q.addData(new Object[]{
                    jobj.getString("description")
            });
        }
        return q.build_cursor();
    }

    protected Query searchSprites(String selection, String[] selectionArgs, String sortOrder)
    throws JSONException, IOException, ParseFail, InterruptedException, ExecutionException {
        Query q = new Query(new String[]{

        });
        return null;
    }

    protected Query searchSpritesBySpecies(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Query searchGames(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    protected Query searchGamesBySpecies(String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Content is read-only.");
    }
}
