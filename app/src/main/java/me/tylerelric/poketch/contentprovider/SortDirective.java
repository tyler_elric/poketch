package me.tylerelric.poketch.contentprovider;

/**
 * Created by Tyler on 12/24/2014.
 */
class SortDirective {

    public final int column;
    public final boolean ascending;

    public SortDirective(int _column,boolean _direction ) {
        column = _column;
        ascending = _direction;
    }

    public static SortDirective[] parseString(String s) {
        return new SortDirective[0];
    }

}
