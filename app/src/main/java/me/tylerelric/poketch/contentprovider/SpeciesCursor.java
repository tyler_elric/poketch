package me.tylerelric.poketch.contentprovider;

import android.database.Cursor;
import android.database.MatrixCursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.tylerelric.pokeapi.PokeApi;

public class SpeciesCursor
extends BaseSubQuery {

    PokeApi service;

    protected static final String[] columnNames = new String[] {
            "name",
            "id",
            "stats.hp",
            "stats.attack",
            "stats.defense",
            "stats.sp_atk",
            "stats.sp_def",
            "stats.speed",
            "catch_rate",
            "egg_cycles",
            "base_exp",
            "height",
            "weight"
    };

    protected static final int[] columnTypes = new int[] {
            FIELD_TYPE_STRING,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER,
            FIELD_TYPE_INTEGER
    };

    public SpeciesCursor(PokeApi _service,Cursor _pokedex, int initialCapacity) {
        super(_pokedex,columnTypes,columnNames,initialCapacity);
        service = _service;
    }

    public SpeciesCursor(PokeApi _service,Cursor _pokedex) {
        super(_pokedex,columnTypes,columnNames);
        service = _service;
    }

    protected Object[] pull_data() {
        String key = super.getString(super.getColumnIndex("key"));
        try {
            JSONObject jobj = service.get(key).get();
            return new Object[]{
                    jobj.getString("name"),
                    jobj.getInt("national_id"),
                    jobj.getInt("hp"),
                    jobj.getInt("attack"),
                    jobj.getInt("defense"),
                    jobj.getInt("sp_atk"),
                    jobj.getInt("sp_def"),
                    jobj.getInt("speed"),
                    jobj.getInt("catch_rate"),
                    jobj.getInt("egg_cycles"),
                    jobj.getInt("exp"),
                    jobj.getInt("height"),
                    jobj.getInt("weight")
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
