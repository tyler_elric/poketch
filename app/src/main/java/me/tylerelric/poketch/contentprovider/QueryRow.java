package me.tylerelric.poketch.contentprovider;

import android.database.MatrixCursor;

/**
 * Created by Tyler on 12/24/2014.
 */
class QueryRow {

    public Object[] data;

    public QueryRow(Object[] _data) {
        data = _data;
    }

    public int evaluateColumn(int c) {
        int r = 0;
        if(data[c] instanceof String) {
            r = evalutateString((String)data[c]);
        } else {
            r = (int) (data[c]);
        }
        return r;
    }

    public int evalutateString(String s) {
        return 0;
    }

    public void insert(MatrixCursor c) {
        c.addRow(data);
    }

}
