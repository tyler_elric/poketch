package me.tylerelric.poketch;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.tylerelric.poketch.contentprovider.QueryResult;

/**
* Created by Tyler on 12/24/2014.
*/
public class ResultAdapter
extends RecyclerView.Adapter<QueryResultView>
implements View.OnClickListener {

    private Cursor cursor = null;
    protected OnResultClicked onResultClicked;
    protected final int kind;

    public ResultAdapter(Cursor cur,int _kind) {
        cursor = cur;
        kind = _kind;
    }

    @Override
    public QueryResultView onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.query_results_item, parent, false);
        v.setOnClickListener(this);
        QueryResultView qrv = new QueryResultView(v);
        return qrv;
    }

    @Override
    public void onBindViewHolder(QueryResultView viewHolder, int i) {
        cursor.moveToPosition(i);
        QueryResult qr = (QueryResult) viewHolder.itemView.getTag();
        if(qr==null) {
            qr = new QueryResult(kind,0);
        }
        switch(kind) {
            case OnResultClicked.TAG_SPECIES_RESULT:
                viewHolder.itemView.setTag(new QueryResult(kind,cursor.getInt(0)));
                bindSpeciesResult(viewHolder);
                break;
            case OnResultClicked.TAG_EGG_GROUP_RESULT:
                String uri = cursor.getString(1);
                while(uri.charAt(uri.length()-1)=='/')
                    uri = uri.substring(0,uri.length()-1);
                int id = Integer.parseInt(uri,10);
                viewHolder.itemView.setTag(new QueryResult(kind,id));
                bindEggGroupResult(viewHolder);
                break;
            case OnResultClicked.TAG_MOVE_RESULT:
                break;
        }
    }

    protected void bindEggGroupResult(QueryResultView viewHolder) {
        viewHolder.updateEggGroupMeta(cursor);
    }

    protected void bindSpeciesResult(QueryResultView viewHolder) {
        viewHolder.updateSpecies(cursor);
    }

    @Override
    public int getItemCount() {
        if(cursor!=null) {
            return cursor.getCount();
        }
        return 0;
    }

    @Override
    public void onClick(View view) {
        if (onResultClicked != null) {
            QueryResult qr = (QueryResult) view.getTag();
            onResultClicked.onResultClicked(qr.kind, qr.id);
        }
    }

    public void setOnResultClickListener(OnResultClicked orc) {
        onResultClicked = orc;
    }

}
