package me.tylerelric.poketch;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends Activity {

    private Handler splash_time = new Handler();
    public static final int MIN_SPLASH_DELAY = 2000;
    private int events = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        splash_time.postDelayed(new Runnable() {
            @Override
            public void run() {
                splashTimeout();
            }
        }, MIN_SPLASH_DELAY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // make sure the pokedex is ready to go.
        // pokedex is ready to go, progress splash screen progress.
        splash_time.postAtFrontOfQueue( new Runnable() {
            @Override
            public void run() {
                downloadPokedex();
                splashTimeout();
            }
        });
    }

    protected void switchToOverview() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    protected void downloadPokedex() {
        ContentResolver mContentResolver = getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://me.tylerelric.poketch.content/pokedex");
        mContentResolver.query(
            uri,
            null,
            null,
            null,
            "Ascending"
        ).close();
    }

    protected void switchToIntro() {

    }

    protected void switchToUpdate() {

    }

    public boolean needsIntroduction() {
        return false;
    }

    public boolean needsUpdate() {
        return false;
    }

    protected void splashTimeout() {
        if(--events<1) {
            if(needsIntroduction()){
                switchToIntro();
            } else if(needsUpdate()){
                switchToUpdate();
            } else {
                switchToOverview();
            }
        }
    }
}
