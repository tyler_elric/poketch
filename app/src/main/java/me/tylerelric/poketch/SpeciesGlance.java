package me.tylerelric.poketch;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SpeciesGlance extends SpeciesDisplaySection {

    TextView speciesNameLabel = null;
    TextView speciesDescriptionLabel = null;
    Cursor description_cursor = null;
    Cursor sprite_cursor = null;

    public SpeciesGlance() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_species_glance,container,false);
        speciesNameLabel = (TextView) view.findViewById(R.id.species_name);
        speciesDescriptionLabel = (TextView) view.findViewById(R.id.species_description);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected void updateDescription() {
        if(description_cursor.getCount()>0){
            description_cursor.moveToLast();
            String description = description_cursor.getString(0);
            Log.i("SpeciesGlance",description);
            speciesDescriptionLabel.setText(description);
        }
    }

    protected void updateSprite() {

    }

    @Override
    public void updateSpecies(Cursor cursor) {
        int species_id = cursor.getInt(1);
        description_cursor = mContentResolver.query(
                Uri.parse("content://me.tylerelric.poketch.content/descriptions/" + species_id),
                null,
                null,
                null,
                "+id"
        );
        /*
        sprite_cursor = mContentResolver.query(
                Uri.parse("content://me.tylerelric.poketch.content/sprites/" + species_id),
                null,
                null,
                null,
                "+id"
        );
        */
        updateSprite();
        updateDescription();
        String speciesName = cursor.getString(0);
        speciesNameLabel.setText(speciesName);
    }

}
