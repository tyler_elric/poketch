package me.tylerelric.poketch;

public interface OnResultClicked {

    public static final int TAG_SPECIES_RESULT = 1;
    public static final int TAG_EGG_GROUP_RESULT = 2;
    public static final int TAG_MOVE_RESULT = 3;

    void onResultClicked(int result_type,int result_value);
}
