package me.tylerelric.poketch;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity
implements OnResultClicked {

    private RecyclerView mRecyclerView;
    private ResultAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ContentResolver mContentResolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContentResolver = getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://me.tylerelric.poketch.content/pokedex");
        Cursor myDataset = mContentResolver.query(
                uri,
                null,
                null,
                null,
                "Ascending"
        );
        Log.w("MainActivity","Results: " + myDataset.getCount());
        mAdapter = new ResultAdapter(myDataset,OnResultClicked.TAG_SPECIES_RESULT);
        mAdapter.setOnResultClickListener(this);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.results_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        setTitle("Pokedex");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void onResultClicked(int result_type, int result_value) {
        switch(result_type) {
            case OnResultClicked.TAG_SPECIES_RESULT:
                Intent i = new Intent(getApplicationContext(),SpeciesDetails.class);
                i.putExtra("species_id",result_value);
                startActivity(i);
                break;
            default:
                break;
        }
    }

}
