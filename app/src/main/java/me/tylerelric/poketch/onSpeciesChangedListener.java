package me.tylerelric.poketch;

import android.database.Cursor;

public interface onSpeciesChangedListener {
    public void updateSpecies(Cursor cursor);
}
