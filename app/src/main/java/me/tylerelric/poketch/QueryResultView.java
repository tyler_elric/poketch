package me.tylerelric.poketch;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.tylerelric.poketch.contentprovider.QueryResult;

public class QueryResultView
extends RecyclerView.ViewHolder {

    protected ImageView icon = null;
    protected TextView label = null;

    public QueryResultView(View v) {
        super(v);
        icon = (ImageView) v.findViewById(R.id.query_result_sprite);
        label = (TextView) v.findViewById(R.id.query_result_label);
    }

    public void updateEggGroupMeta(Cursor cursor) {
        setLabel(cursor.getString(0));
        icon.setImageResource(R.drawable.eggicon);
        return;
    }

    public void updateSpecies(Cursor cursor) {
        setLabel(cursor.getString(1));
        icon.setImageResource(R.drawable.ic_launcher);
    }

    public void setIcon(Drawable drawable) {
        icon.setImageDrawable(drawable);
    }

    public void setLabel(String msg) {
        label.setText(msg);
    }

}
