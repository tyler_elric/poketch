package me.tylerelric.pokeapi;

import android.util.Log;

import org.json.JSONObject;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

public class JSONCache
extends DiskCache<JSONObject> {

	public JSONCache(File dir,int max_size) {
		super(dir,max_size);
	}

	public JSONObject decompile(String data) throws ParseFail {
		try {
			return new JSONObject(data);
		} catch( JSONException e ) {
            Log.i("JSONCache", data);
			throw new ParseFail(e);
		}
	}

	@Override
	public String make_filename(String key) {
		return super.make_filename(key)+".json";
	}
}