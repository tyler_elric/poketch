package me.tylerelric.pokeapi;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Wall<V>
{
	protected ArrayList<String> to_get = new ArrayList<String>();
	protected List<V> retrieved = new ArrayList<V>();

	protected DiskCache<V> service;

	public Wall(DiskCache<V> _service) {
		service = _service;
	}

	public Object prepare(String key) {
		to_get.add(key);
		return null;
	}

	public V[] get_all()
	throws ParseFail, IOException, InterruptedException, ExecutionException {
		ArrayList<Future<V>> pending = new ArrayList<Future<V>>();
		for(int i=0;i<to_get.size();i++) {
			pending.add(service.get(to_get.get(i)));
		}
		for(int i=0;i<to_get.size();i++) {
			retrieved.add(pending.get(i).get());
		}
		return (V[]) retrieved.toArray();
	}

    public int size() {
        return to_get.size();
    }

    public V get_one(int i)
    throws ParseFail, IOException, InterruptedException, ExecutionException {
        if(retrieved.size()>i) {
            V x = retrieved.get(i);
            if(x!=null) {
                return x;
            }
        } else {
            while(retrieved.size()<=i) {
                retrieved.add(null);
            }
        }
        Future<V> f = service.get(to_get.get(i));
        //to_get.remove(i);
        V x = f.get();
        retrieved.add(i,x);
        return x;
    }

    public  V get_one()
    throws ParseFail, IOException, InterruptedException, ExecutionException {
        return get_one(0);
    }
}