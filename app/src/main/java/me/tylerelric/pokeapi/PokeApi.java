package me.tylerelric.pokeapi;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.io.IOException;

public class PokeApi
extends WebServiceCache {
	
	public PokeApi(File directory,int max) {
		super("http://pokeapi.co/api/v1/",directory,max);
	}

	public JSONObject getPokedex()
    throws IOException, ParseFail, InterruptedException, ExecutionException
    {
		return get("pokedex/1").get();
	}

	public JSONObject getSpecies(int species_id)
    throws IOException, ParseFail, InterruptedException, ExecutionException
    {
		return get("pokemon/" + species_id).get();
	}

	public JSONObject getMove(int move_id)
    throws IOException, ParseFail, InterruptedException, ExecutionException
    {
		return get("move/" + move_id).get();
	}

	public static void OutputURIs(PokeApi service, String[] args) {
		for(String uri:args) {
			try {
				System.out.println("Getting resource: "+ uri);
				System.out.println(service.get(uri).toString());
			} catch( ParseFail e ) {
				System.out.println("Error parsing: " + uri);
				System.out.println(e.toString());
			} catch (IOException e) {
				System.out.println("Error downloading: " + uri);
				System.out.println(e.toString());
			} catch (InterruptedException e) {
				System.out.println("InterruptedException: " + uri);
				System.out.println(e.toString());
			} /*catch (ExecutionException e) {
				System.out.println("InterruptedException: " + uri);
				System.out.println(e.toString());
			}*/
			System.out.println();
		}
	}

	public static void OutputSpeciesNames(PokeApi service) {
		JSONObject pokedex = null;
		JSONArray species_list = null;

		/* Get the pokedex */
		try {
			pokedex = service.getPokedex();
		} catch(IOException e) {
			System.out.println("Downloading error:");
			System.out.println(e.toString());
		} catch (ParseFail e) {
			System.out.println("Parsing Failure");
			System.out.println(e.toString());
		} catch (InterruptedException e ) {
			System.out.println("InterruptedException: ");
			System.out.println(e.toString());
		} catch (ExecutionException e) {
			System.out.println("InterruptedException: ");
			System.out.println(e.toString());
		}
		if(pokedex==null) {
			return;
		}

		/* Get the species list */
		try {
			species_list = pokedex.getJSONArray("pokemon");
		} catch (JSONException e) {
			System.out.println("Parsing Failure:");
			System.out.println(e.toString());
		}

		if(species_list==null) {
			return;
		}

		/* Build the Wall */
		Wall<JSONObject> wall= new Wall<JSONObject>(service);
		int prefix = "api/v1/".length();
		for(int i=0;i<species_list.length();i++) {
			try {
				JSONObject meta = species_list.getJSONObject(i);
				String uri = meta.getString("resource_uri").substring(prefix);
				wall.prepare(uri);
				//System.out.println(uri);
			} catch(JSONException e) {
				System.out.println("JSON Exception:");
				System.out.println(e);
			}
		}

		try {
			Object[] pokemon = (Object[]) wall.get_all();
			for(Object obj:pokemon) {
				try {
					JSONObject jobj = (JSONObject)obj;
					System.out.println(jobj.getString("name"));
				} catch(JSONException e){
					System.out.println("JSON Exception:");
					System.out.println(e);
				} catch(Exception e) {
					System.out.println("Uncaught error in parsing species:");
					System.out.println(e.getMessage());
				}
			}
		} catch(ParseFail e) {
			System.out.println("Uncaught error:");
			System.out.println(e.getMessage());
		} catch(IOException e) {
			System.out.println("Downloading error:");
			System.out.println(e.getMessage());
		} catch(InterruptedException e) {
			System.out.println("InterruptedException: ");
			System.out.println(e.toString());
		} catch (ExecutionException e) {
			System.out.println("InterruptedException: ");
			System.out.println(e.toString());
		}
	}

	public static void main(String[] args) {
		PokeApi service = null;
		try {
            File dir = new File("pokeapi");
			service = new PokeApi(dir,128);
			OutputSpeciesNames(service);
		} finally {
			if(service!=null) {
				service.dispose();	
			}
		}
	}

}