package me.tylerelric.pokeapi;

public class ParseFail
extends Exception
{
	public ParseFail(Exception e){
		super(e);
	}
}