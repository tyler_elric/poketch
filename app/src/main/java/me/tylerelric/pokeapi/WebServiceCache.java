package me.tylerelric.pokeapi;

import org.json.JSONObject;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class WebServiceCache
extends JSONCache {

	protected final String service_location;

	public WebServiceCache(String _service_location,File directory,int max) {
		super(directory,max);
		service_location = _service_location;
	}

	@Override
	public JSONObject load(String key) throws ParseFail, IOException {
		try {
			return super.load(key);
		} catch (IOException e) {
			URL url = new URL(service_location + key);
			BufferedReader reader = null;
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			try {
				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line = null;
				StringBuffer fileContents = new StringBuffer();
				while ((line=reader.readLine()) != null) {
					fileContents.append(line);
				}
				JSONObject jobj = decompile(fileContents.toString());
				save(key,jobj);
				return jobj;
			} finally {
				conn.disconnect();
			}
		}
	}

	@Override
	public String make_filename(String key) {
		key = key.replaceAll("/$","");
		key = key.replaceAll("\\.+",".");
		return super.make_filename(key);
	}
}
