package me.tylerelric.pokeapi;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.Set;
import java.util.Collection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.FutureTask;
import java.lang.InterruptedException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public abstract class DiskCache<V>
{

	ExecutorService executor = Executors.newFixedThreadPool(5);

	protected class ThreadedAccesor<V>
	implements Callable<V>
	{
		DiskCache<V> cache;
		String index;
		public ThreadedAccesor(DiskCache<V> inst,String key) {
			cache = inst;
			index = key;
		}

		@Override
		public V call() throws IOException, ParseFail {
			return cache.obtainInstance(index);
		}
	}

	final File directory;
	final int max_size;

	protected ConcurrentHashMap<String,V> data = new ConcurrentHashMap<String,V>();
	protected ConcurrentHashMap<String,Integer> frequency = new ConcurrentHashMap<String,Integer>();

	public DiskCache(File _directory,int _max_size) {
		directory = _directory;
		max_size = _max_size;
        directory.mkdirs();
	}

	public boolean containsValue(V val) {
		return data.containsValue(val);
	}

	public boolean containsKey(String key) {
		boolean r = data.containsKey(key);
		if(r) {
			return true;
		}
		String fn = make_filename(key);

		return false;
	}

	public Future<V> get(String key) throws IOException, ParseFail, InterruptedException {
		return executor.submit(new ThreadedAccesor(this,key));
	}

	public int getSize() {
		return data.size();
	}

	public boolean isEmpty() {
		return getSize()<1;
	}

	public void put(String key,V value) throws IOException {
		save(key,value);
		data.put(key,value);
		frequency.put(key,1);
	}

	protected void prune() {
		if(getSize()>=max_size) {
			int[] _frequency = new int[getSize()];
			int min_frequency = 0;
			for(Integer vf:frequency.values()) {
				min_frequency = Integer.compare(min_frequency,vf)<0?min_frequency:vf;
			}
			for(String key:frequency.keySet()) {
				if(frequency.get(key)==min_frequency) {
					data.remove(key);
				}
			}
		}
	}

	public String make_filename(String x) {
		return x;
	}

	protected void save(String key,V value) throws IOException {
		String filename = make_filename(key);
		System.out.println("Saving " + key + " to " + filename);
		File file = new File(directory,filename);
		if(file.exists()) {
			file.delete();
			file.createNewFile();
		} else {
            // make sure the directory exists.
            // useful if key has a directory separator in it.
            // new File(file.getAbsolutePath()).mkdirs();
            File parent = file.getParentFile();
            parent.mkdirs();
        }
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		String data = compile(value);
		fw.write(data,0,data.length());
		fw.close();
	}

	protected V load(String key) throws IOException, ParseFail {
		String filename = make_filename(key);
		File file = new File(directory,filename);
		BufferedReader br = new BufferedReader(new FileReader(file));
		StringBuffer fileContents = new StringBuffer();
		String line = br.readLine();
		while (line != null) {
			fileContents.append(line);
			line = br.readLine();
		}
		return decompile(fileContents.toString());
	}

	public V obtainInstance(String key) throws IOException, ParseFail {
		if(containsKey(key)) {
			frequency.put(key,frequency.get(key)+1);
			return data.get(key);
		} else {
			V r = load(key);
			prune();
			// use this.put() only when the data-item
			// doesn't already exist. otherwise this
			// would end up being recursive I think.
			// or something. I just feel like using 
			// this.put() would lead to having issues.
			frequency.put(key,1);
			data.put(key,r);
			return r;
		}
	}

	public abstract V decompile(String data) throws ParseFail;

	public String compile(V value) {
		return value.toString();
	}

	public static int min(int[] data) {
		Arrays.sort(data);
		return data[0];
	}

	public static int max(Integer[] data) {
		Arrays.sort(data);
		return data[data.length-1];
	}

	public void dispose() {
		executor.shutdown();
	}

}
